package com.example.messageBroker.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class CampaignClickData {
  private String userId;
  private String campaignId;
  private Date clickedAt;
}
