package com.example.messageBroker.utils;

import com.example.messageBroker.core.broker.iface.Publisher;
import com.example.messageBroker.core.dto.JsonMessage;
import com.example.messageBroker.core.dto.Message;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Date;
import java.util.UUID;

@Slf4j
public  class RandomMessageGenerator extends Thread{

  private final Publisher<Message> publisher;
  private final int count;

  public RandomMessageGenerator(
      Publisher<Message> producer, int count) {
    this.publisher = producer;
    this.count = count;
  }

  @SneakyThrows
  @Override
  public void run() {
    for(int i = 1; i <=count; i++) {
      Thread.sleep(500);
      Message message = createRandomMessage();
      log.info("publishing message for queue = {}, message = {}", publisher.getQueueName(), message.getData());
      publisher.publishMessage(message);
    }
  }

  public Message createRandomMessage() {
    Gson gson = new Gson();

    return new JsonMessage(
        UUID.randomUUID().toString(),
        gson.toJson(
            new CampaignClickData(RandomStringUtils.randomAlphabetic(5),
                RandomStringUtils.randomAlphabetic(10), new Date()))
    );
  }
}
