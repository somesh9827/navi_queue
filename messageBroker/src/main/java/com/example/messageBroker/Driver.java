package com.example.messageBroker;

import com.example.messageBroker.core.broker.iface.MessageBroker;
import com.example.messageBroker.core.broker.iface.Publisher;
import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.dto.SubscriberType;
import com.example.messageBroker.core.queue.QueueType;
import com.example.messageBroker.core.dto.SubscriptionInfo;
import com.example.messageBroker.utils.RandomMessageGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Scanner;
import javax.annotation.PostConstruct;

@Service
@Slf4j
public class Driver {

  @Autowired
  private MessageBroker messageBroker;

  @PostConstruct
  public void run() throws InterruptedException {
    InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("input.txt");
    Scanner scanner = new Scanner(inputStream);
    while (scanner.hasNextLine()) {
      String[] args = scanner.nextLine().split(" ");
      switch(args[0]) {
        case "create_queue" :
          createQueue(args[1]);
          break;
        case "subscribe" :
          subscribeQueue(args[1], args[2], args[3], HttpMethod.resolve(args[4]));
          break;
        case "publish_random" :
          publishRandomMessage(args[1], Integer.parseInt(args[2]));
          break;
        case "unsubscribe" :
          unsubscribe(args[1], args[2]);
          break;
      }
    }

    log.info("Completed Successfully !!");

  }

  private void createQueue(String queueName) {
    log.info("creating queue {}", queueName);
    messageBroker.createQueue(queueName, QueueType.IN_MEMORY_QUEUE);
  }

  private void subscribeQueue(String queueName, String subscriberId,String apiUri, HttpMethod method) {
    log.info("subscriber {} is subscribing for queue {}", subscriberId, queueName);
    messageBroker.subscribe(new SubscriptionInfo(queueName, apiUri,
        method, subscriberId, SubscriberType.HTTP_SUBSCRIBER));
  }

  private void publishRandomMessage(String queueName, int count) {
    Publisher<Message> messagePublisher = messageBroker.getPublisher(queueName);
    RandomMessageGenerator randomMessageGenerator = new RandomMessageGenerator(messagePublisher, count);
    randomMessageGenerator.start();
  }

  private void unsubscribe(String queueName, String subscriberId) throws InterruptedException {
    Thread.sleep(5000);
    messageBroker.unsubscribe(queueName, subscriberId);
    log.info(subscriberId + " unsubscribe from queue " + queueName);
  }
}
