package com.example.messageBroker.core.queue;

import com.example.messageBroker.core.dto.Message;

import java.util.Iterator;

public interface AbstractQueueIterator<T extends Message> extends Iterator<T> {
  void updateOffset(int offset);

}
