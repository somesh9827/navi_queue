package com.example.messageBroker.core.broker.iface;

import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.queue.AbstractQueueIterator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Consumer<T extends Message> extends Thread {
  protected final AbstractQueueIterator<T> iterator;
  protected MessageConsumerErrorHandeler messageConsumerErrorHandeler =  null;
  private boolean shutDown = false;
  public Consumer(AbstractQueueIterator<T> iterator) {
    this.iterator = iterator;
  }

  @Override
  public void run() {
    while (!shutDown) {
      T t =  iterator.next();
      consumeMessage(t);
    }
  }

  public void shutDown() {
    this.shutDown = true;
  }

  protected abstract void consumeMessage(T t);

}
