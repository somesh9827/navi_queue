package com.example.messageBroker.core.broker.iface;

import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.queue.AbstractQueue;
import com.example.messageBroker.core.queue.QueueType;

import java.util.Optional;

public interface QueueManager<T extends Message> {

  AbstractQueue<Message> createQueue(String name, QueueType type);
  Optional<AbstractQueue<Message>> getQueue(String name);
}
