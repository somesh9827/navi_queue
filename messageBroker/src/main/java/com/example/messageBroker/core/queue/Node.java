package com.example.messageBroker.core.queue;

import com.example.messageBroker.core.dto.Message;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Node<T extends Message> {
  private T message;
  private Node<T> next;

  public Node(T message) {
    this.message = message;
    this.next = null;
  }

  public Node() {
    this.message = null;
    this.next = null;
  }

  public void setMessage(T message) {
    this.message = message;
  }

  public void setNext(Node<T> next) {
    this.next = next;
  }
}
