package com.example.messageBroker.core.dto;

import com.example.messageBroker.core.dto.SubscriberType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpMethod;

@Getter
@Setter
@AllArgsConstructor
public class SubscriptionInfo {
    private String queueName;
    private String apiUrl;
    private HttpMethod httpMethod;
    private String subscriberId;
    private SubscriberType subscriberType;
}
