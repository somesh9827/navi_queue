package com.example.messageBroker.core.broker.iface;

import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.queue.QueueType;
import com.example.messageBroker.core.dto.SubscriptionInfo;

public interface MessageBroker {

  void createQueue(String queueName, QueueType type);
  void subscribe(SubscriptionInfo createSubscriptionDto);
  void unsubscribe(String queueName, String subscriberId);
  // I have used this method so that there will be one publisher for a queue(Requirement 3)
  Publisher<Message> getPublisher(String queueName);

}
