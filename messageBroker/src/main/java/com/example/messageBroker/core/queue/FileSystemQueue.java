package com.example.messageBroker.core.queue;

import com.example.messageBroker.core.dto.Message;

//Not implemented This functionality right now.
// It is just showcase different type of Queue
public class FileSystemQueue <T extends Message> implements AbstractQueue<T>{

  private final String name;

  public FileSystemQueue(String name) {
    this.name = name;
  }

  @Override
  public int size() {
    return 0;
  }

  @Override
  public void enqueue(T message) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public AbstractQueueIterator<T> iterator() {
    return null;
  }
}
