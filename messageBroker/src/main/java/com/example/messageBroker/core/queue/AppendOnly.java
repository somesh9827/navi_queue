package com.example.messageBroker.core.queue;
/*
* This is marker interface to indicate the no deletion or random write allowed.
* */
public interface AppendOnly{

}
