package com.example.messageBroker.core.broker;

import com.example.messageBroker.core.broker.iface.QueueManager;
import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.queue.AbstractQueue;
import com.example.messageBroker.core.queue.QueueType;
import com.example.messageBroker.exception.ResourceAlreadyExistException;
import lombok.RequiredArgsConstructor;
import lombok.Synchronized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class QueueManagerImpl implements QueueManager {

  private Map<String, AbstractQueue<Message>> queueMap = new ConcurrentHashMap<>();


  @Override
  @Synchronized
  public AbstractQueue<Message> createQueue(String name, QueueType type) {
    validateQueueName(name);
    AbstractQueue<Message> queue = QueueFactory.createQueue(name, type);
    queueMap.put(name,queue);
    return queue;
  }

  private void validateQueueName(String name) {
    if(queueMap.containsKey(name))
      throw new ResourceAlreadyExistException("queue with queue name = " + name + " already exist");
  }

  @Override
  public Optional<AbstractQueue<Message>> getQueue(String name) {
    return Optional.ofNullable(queueMap.get(name));
  }

}
