package com.example.messageBroker.core.broker;

import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.queue.AbstractQueue;
import com.example.messageBroker.core.queue.FileSystemQueue;
import com.example.messageBroker.core.queue.InMemoryQueue;
import com.example.messageBroker.core.queue.QueueType;

public  class  QueueFactory<T> {
    static AbstractQueue<Message>  createQueue(String name, QueueType type) {
      switch (type) {
        case IN_MEMORY_QUEUE:
          return new InMemoryQueue<>(name);
        case FILE_SYSTEM_QUEUE:
          return new FileSystemQueue<>(name);
        default:
          return new InMemoryQueue<>(name);
      }
    }
}
