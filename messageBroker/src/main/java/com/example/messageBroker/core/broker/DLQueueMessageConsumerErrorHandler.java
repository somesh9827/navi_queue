package com.example.messageBroker.core.broker;

import com.example.messageBroker.core.broker.iface.MessageConsumerErrorHandeler;
import com.example.messageBroker.core.broker.iface.Publisher;
import com.example.messageBroker.core.dto.Message;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DLQueueMessageConsumerErrorHandler implements MessageConsumerErrorHandeler {
  private final Publisher<Message> dlQueuePublisher;
  private final String mainQueueName;
  private final String dlQueueName;

  public DLQueueMessageConsumerErrorHandler(
      Publisher<Message> dlQueuePublisher, String mainQueueName, String dlQueueName) {
    this.dlQueuePublisher = dlQueuePublisher;
    this.mainQueueName = mainQueueName;
    this.dlQueueName = dlQueueName;
  }

  @Override
  public void handleNonConsumeMessage(Message message) {
    log.info("putting the message  to DL Queue = {} from main queue = {} {}", dlQueueName, mainQueueName, message.getData());
    dlQueuePublisher.publishMessage(message);
  }
}
