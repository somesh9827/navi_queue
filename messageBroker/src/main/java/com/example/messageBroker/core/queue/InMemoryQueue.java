package com.example.messageBroker.core.queue;

import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.exception.OffsetOutOfBoundException;
import com.example.messageBroker.exception.QueueFullException;
import lombok.SneakyThrows;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class InMemoryQueue<T extends Message> implements AbstractQueue<T> {

  private int size;
  private final Node<T> head;
  private Node<T> front;
  private final int capacity;
  private final String name;

  ReentrantLock lock = new ReentrantLock();
  Condition noMoreElement = lock.newCondition();

  public InMemoryQueue(String name) {
    this.capacity = Integer.MAX_VALUE;
    this.size = 0;
    head = new Node<T>();
    front = head;
    this.name = name;

  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void enqueue(T message) {
    checkIsFull();
    Node<T> node = new Node<T>(message);
    lock.lock();
    try {
      front.setNext(node);
      front = front.getNext();
      size++;
    } finally {
      noMoreElement.signalAll();
      lock.unlock();
    }
  }

  //Currently retention policy is not implemented that why throwing an error
  private void checkIsFull() {
    if (this.capacity == this.size) {
      throw new QueueFullException("queue is full");
    }
  }

  @Override
  public AbstractQueueIterator<T> iterator() {

    return new InMemoryQueueIterator<>(head);
  }

  private class InMemoryQueueIterator<T extends Message> implements AbstractQueueIterator<T> {

    private final Node<T> head;
    private Node<T> current;
    private int currentIndex;

    InMemoryQueueIterator(Node<T> head) {
      this.head = head;
      this.current = head;
      this.currentIndex = 0;
    }

    @Override
    public boolean hasNext() {
      return current != null;
    }

    @SneakyThrows
    @Override
    public T next() {
      lock.lock();
      try {
        while (currentIndex == size) {
          noMoreElement.await();
        }

        currentIndex++;
        T t = current.getNext().getMessage();
        current = current.getNext();
        return t;
      } finally {
        lock.unlock();
      }
    }

    @Override
    public void updateOffset(int offset) {
      if (offset > size) {
        throw new OffsetOutOfBoundException("Offset is beyond the size of queue");
      }
      Node<T> temp = head;
      int i = 0;
      while (i <= offset) {
        temp = temp.getNext();
        i++;
      }
      current = temp;
    }
  }
}