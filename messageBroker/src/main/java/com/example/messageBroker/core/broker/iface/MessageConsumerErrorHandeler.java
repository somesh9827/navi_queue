package com.example.messageBroker.core.broker.iface;

import com.example.messageBroker.core.dto.Message;

public interface MessageConsumerErrorHandeler {

    void handleNonConsumeMessage(Message message);
}
