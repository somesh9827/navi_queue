package com.example.messageBroker.core.broker.iface;

import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.queue.AbstractQueue;

public class Publisher<T extends Message> {
  private final AbstractQueue<T> queue;

  public Publisher(AbstractQueue<T> queue) {
    this.queue = queue;
  }

  public void publishMessage(T t) {
    queue.enqueue(t);
  }

  public String getQueueName() {
    return queue.getName();
  }
}
