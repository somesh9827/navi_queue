package com.example.messageBroker.core.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JsonMessage extends Message{

  public JsonMessage(String id, String data) {
    super(id, data);
  }

  @Override
  long size() {
    return data.getBytes().length;
  }
}
