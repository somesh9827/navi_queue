package com.example.messageBroker.core.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ConsumerConfig {
  private String id;
  private SubscriptionInfo subscriptionDto;
}
