package com.example.messageBroker.core.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public abstract class Message {
  protected String id;
  protected String data;

  abstract long size();
}
