package com.example.messageBroker.core.broker;

import com.example.messageBroker.core.broker.iface.Consumer;
import com.example.messageBroker.core.broker.iface.HttpAPIConsumer;
import com.example.messageBroker.core.broker.iface.MessageBroker;
import com.example.messageBroker.core.broker.iface.MessageConsumerErrorHandeler;
import com.example.messageBroker.core.broker.iface.Publisher;
import com.example.messageBroker.core.broker.iface.QueueManager;
import com.example.messageBroker.core.dto.ConsumerConfig;
import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.queue.AbstractQueue;
import com.example.messageBroker.core.queue.AbstractQueueIterator;
import com.example.messageBroker.core.queue.QueueType;
import com.example.messageBroker.core.dto.SubscriptionInfo;
import com.example.messageBroker.exception.ResourceNotExistException;
import com.example.messageBroker.exception.SubscriberAlreadyExistException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class MessageBrokerImpl implements MessageBroker {

  private final QueueManager<Message> queueManager;
  private final HashMap<String, Publisher<Message>> queueToPublisherMap = new HashMap<>();
  private final Map<String, Consumer<Message>> subscriberToConsumerMapping = new HashMap<>();

  private final RestTemplate restTemplate;

  public MessageBrokerImpl(QueueManager<Message> queueManager, RestTemplate restTemplate) {
    this.queueManager = queueManager;
    this.restTemplate = restTemplate;
  }

  @Override
  public void createQueue(String queueName, QueueType type) {
    queueManager.createQueue(queueName, type);
  }

  @Override
  public void subscribe(SubscriptionInfo subscriptionInfo) {
    String key = subscriptionInfo.getSubscriberId() + "_" + subscriptionInfo.getQueueName();
    if (subscriberToConsumerMapping.containsKey(key)) {
      throw new SubscriberAlreadyExistException(
          String.format("subscriber with subscriber = %s and queue %s already exist",
              subscriptionInfo.getSubscriberId(), subscriptionInfo.getQueueName()));
    }
    AbstractQueue<Message> queue = queueManager
        .getQueue(subscriptionInfo.getQueueName()).orElseThrow(() -> new ResourceNotExistException("queue = " +  subscriptionInfo.getQueueName() + "not exist"));

    Consumer<Message> consumer = createHttpConsumer(subscriptionInfo, queue);
    subscriberToConsumerMapping.put(key, consumer);

    consumer.start();
  }

  @Override
  public void unsubscribe(String queueName, String subscriberId) {
    String key = subscriberId + "_" + queueName;
    if (!subscriberToConsumerMapping.containsKey(key)) {
      throw new ResourceNotExistException(
          String.format("subscriber with subcriber = %s and queue %s not exist",
              subscriberId, queueName));
    }
    Consumer<Message> consumer = subscriberToConsumerMapping.get(key);
    consumer.shutDown();
    subscriberToConsumerMapping.remove(key);
    log.info("subscriber = {} unsubscribed succesfully from queue name = {}", subscriberId, queueName);
  }

  // I have used this method so that there will be one publisher for a queue(Requirement 3)
  @Override
  public Publisher<Message> getPublisher(String queueName) {
    if (!queueToPublisherMap.containsKey(queueName)) {
      queueToPublisherMap.put(queueName, new Publisher<>(queueManager.getQueue(queueName).get()));
    }
    return queueToPublisherMap.get(queueName);
  }

  //This method can be come under Consumer Factory, where we can create different type of consumer;
  private Consumer<Message> createHttpConsumer(SubscriptionInfo subscriptionInfo, AbstractQueue<Message> abstractQueue) {

    MessageConsumerErrorHandeler messageConsumerErrorHandeler = new DLQueueMessageConsumerErrorHandler(
        new Publisher<>(createDlQueue(getDlQueueName(abstractQueue.getName(), subscriptionInfo.getSubscriberId()), QueueType.IN_MEMORY_QUEUE)),
        abstractQueue.getName(), getDlQueueName(abstractQueue.getName(), subscriptionInfo.getSubscriberId())
    );

    ConsumerConfig consumerConfig = new ConsumerConfig(subscriptionInfo.getSubscriberId(), subscriptionInfo);
    AbstractQueueIterator<Message> abstractQueueIterator = abstractQueue.iterator();
    Consumer<Message> consumer = new HttpAPIConsumer<>(consumerConfig,abstractQueueIterator, restTemplate, messageConsumerErrorHandeler);
    return consumer;
  }

  private AbstractQueue<Message> createDlQueue(String dlQueueName, QueueType queueType) {
    return queueManager.createQueue(dlQueueName, queueType);
  }

  private String getDlQueueName(String mainQueueName, String subscriberId) {
    return "dl_" + mainQueueName +"_" + subscriberId;

  }

}
