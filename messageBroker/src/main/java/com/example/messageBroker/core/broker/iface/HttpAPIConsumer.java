package com.example.messageBroker.core.broker.iface;

import com.example.messageBroker.core.dto.ConsumerConfig;
import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.queue.AbstractQueueIterator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
public class HttpAPIConsumer<T extends Message> extends Consumer<T>{

  private final ConsumerConfig consumerConfig;
  private final RestTemplate restTemplate;

  public HttpAPIConsumer(ConsumerConfig consumerConfig,
      AbstractQueueIterator<T> iterator, RestTemplate restTemplate) {
    super(iterator);
    this.consumerConfig = consumerConfig;
    this.restTemplate = restTemplate;
  }


  public HttpAPIConsumer(ConsumerConfig consumerConfig,
      AbstractQueueIterator<T> iterator, RestTemplate restTemplate, MessageConsumerErrorHandeler messageConsumerErrorHandeler) {
    super(iterator);
    this.consumerConfig = consumerConfig;
    this.restTemplate = restTemplate;
    super.setMessageConsumerErrorHandeler(messageConsumerErrorHandeler);
  }


  @Override
  protected void consumeMessage(T message) {

    log.info("Consuming message from consumer = {} is {} ", consumerConfig.getId(), message.getData());

    UriComponentsBuilder builder = UriComponentsBuilder
        .fromHttpUrl(consumerConfig.getSubscriptionDto().getApiUrl());
    callService(builder, consumerConfig.getSubscriptionDto().getHttpMethod(), message);

  }

  public void callService(
      UriComponentsBuilder builder,HttpMethod httpMethod, T message) {
    ResponseEntity<String> responseEntity = null;
    HttpEntity<Object> entity = new HttpEntity<>(message, new HttpHeaders());
      try {
        log.info("Calling external Service {}", builder.toUriString());
        responseEntity = restTemplate.exchange(builder.toUriString(), httpMethod,
            entity, String.class);
      }catch (RestClientException ex) {
        onRestClientException(message);
      }
  }

  private void onRestClientException(T message) {
    if(messageConsumerErrorHandeler != null)
      messageConsumerErrorHandeler.handleNonConsumeMessage(message);
    else
      log.info("skipping the message since error handler is not provided. {}", message.getData());
  }

}
