package com.example.messageBroker.core.queue;

public enum  QueueType {
  IN_MEMORY_QUEUE, FILE_SYSTEM_QUEUE
}
