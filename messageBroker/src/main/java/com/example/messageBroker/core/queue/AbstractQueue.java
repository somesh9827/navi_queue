package com.example.messageBroker.core.queue;

import com.example.messageBroker.core.dto.Message;

public interface AbstractQueue<T extends Message> extends AppendOnly, SerialAccess{

  int size();

  String getName();

  void enqueue(T message);

  AbstractQueueIterator<T> iterator();

}
