package com.example.messageBroker.core.queue;
/*
* This is marker interface, which indicates that only serial access of message is allowed. Random access is not allowed
* */
public interface SerialAccess {

}
