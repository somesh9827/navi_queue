package com.example.messageBroker.exception;

public class QueueIsEmptyException extends RuntimeException{

  public QueueIsEmptyException() {
  }

  public QueueIsEmptyException(String message) {
    super(message);
  }

  public QueueIsEmptyException(String message, Throwable cause) {
    super(message, cause);
  }

  public QueueIsEmptyException(Throwable cause) {
    super(cause);
  }

  public QueueIsEmptyException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
