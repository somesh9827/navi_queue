package com.example.messageBroker.exception;

public class SubscriberAlreadyExistException extends RuntimeException{

  public SubscriberAlreadyExistException() {
  }

  public SubscriberAlreadyExistException(String message) {
    super(message);
  }

  public SubscriberAlreadyExistException(String message, Throwable cause) {
    super(message, cause);
  }

  public SubscriberAlreadyExistException(Throwable cause) {
    super(cause);
  }

  public SubscriberAlreadyExistException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
