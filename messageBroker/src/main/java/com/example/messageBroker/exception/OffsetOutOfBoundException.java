package com.example.messageBroker.exception;

public class OffsetOutOfBoundException extends RuntimeException{

  public OffsetOutOfBoundException() {
    super();
  }

  public OffsetOutOfBoundException(String message) {
    super(message);
  }

  public OffsetOutOfBoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public OffsetOutOfBoundException(Throwable cause) {
    super(cause);
  }

  protected OffsetOutOfBoundException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
