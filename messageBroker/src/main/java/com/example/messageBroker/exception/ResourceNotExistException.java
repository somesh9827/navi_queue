package com.example.messageBroker.exception;

public class ResourceNotExistException extends RuntimeException {

  public ResourceNotExistException() {
    super();
  }

  public ResourceNotExistException(String message) {
    super(message);
  }

  public ResourceNotExistException(String message, Throwable cause) {
    super(message, cause);
  }

  public ResourceNotExistException(Throwable cause) {
    super(cause);
  }

  protected ResourceNotExistException(String message, Throwable cause,
      boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
