package com.example.messageBroker.core.queue;

import com.example.messageBroker.core.dto.JsonMessage;
import com.example.messageBroker.core.dto.Message;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.test.context.event.annotation.BeforeTestMethod;

import java.lang.Thread.State;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import static org.junit.Assert.assertEquals;

public class InMemoryImplTest {


  private final String queueName = "default_queue";
  @InjectMocks
  private InMemoryQueue<Message> inMemoryQueue;

  @Before
  public void init() {
    inMemoryQueue = new InMemoryQueue<>(queueName);
  }

  @BeforeTestMethod
  public void initBeforeMethod() {
    inMemoryQueue = new InMemoryQueue<>(queueName);
  }


  @Test
  public void testEnqueue() {
    inMemoryQueue.enqueue(createMessage());
    assertEquals(inMemoryQueue.size(), 1);
  }

  @Test
  public void testEnqueueWhenMultiplePublisherPublishingTheMessage() {
    String publisher1Name = "p1";
    String publisher2Name = "p2";
    Map<String, Boolean> publisherCompletionNotifier = new HashMap<>();
    publisherCompletionNotifier.put(publisher1Name, false);
    publisherCompletionNotifier.put(publisher2Name, false);
    BulkPublisher bulkPublisher1 = new BulkPublisher(publisher1Name, inMemoryQueue,publisherCompletionNotifier, 100);
    BulkPublisher bulkPublisher2 = new BulkPublisher(publisher2Name, inMemoryQueue,publisherCompletionNotifier, 50);
    bulkPublisher1.start();
    bulkPublisher2.start();
    while (!publisherCompletionNotifier.get(publisher1Name) || !publisherCompletionNotifier.get(publisher2Name))
      continue;

    assertEquals(inMemoryQueue.size(), 150);
  }

  @Test
  public void testIteratorNextShouldBlockWhenNoMoreMessagePresentInQueue() throws Exception{
    Consumer c1 = new Consumer(inMemoryQueue.iterator());
    c1.start();
    Thread.sleep(1000);
    assertEquals(State.WAITING, c1.getState());
  }

  @Test
  public void testQueueWhenBothPublisherAndConsumerRunningTogetherThenConsumerShouldWaitAfterPublisherFinish() throws Exception{
    String publisher1Name = "p1";
    Map<String, Boolean> publisherCompletionNotifier = new HashMap<>();
    publisherCompletionNotifier.put(publisher1Name, false);
    BulkPublisher bulkPublisher1 = new BulkPublisher(publisher1Name, inMemoryQueue,publisherCompletionNotifier, 100);
    Consumer c1 = new Consumer(inMemoryQueue.iterator());
    bulkPublisher1.start();
    c1.start();

    Thread.sleep(10000);
    assertEquals(State.WAITING, c1.getState());
    assertEquals(c1.messageConsumptionCount, 100);
  }

  @Test
  public void testQueueWhenMultipleConsumerAreRunning() throws Exception{
    String publisher1Name = "p1";
    Map<String, Boolean> publisherCompletionNotifier = new HashMap<>();
    publisherCompletionNotifier.put(publisher1Name, false);
    BulkPublisher bulkPublisher1 = new BulkPublisher(publisher1Name, inMemoryQueue,publisherCompletionNotifier, 100);
    Consumer c1 = new Consumer(inMemoryQueue.iterator());
    Consumer c2 = new Consumer(inMemoryQueue.iterator());
    bulkPublisher1.start();
    c1.start();
    c2.start();
    while (!bulkPublisher1.getState().equals(State.TERMINATED))continue;
    Thread.sleep(1000);
    assertEquals(State.WAITING, c1.getState());
    assertEquals(State.WAITING, c2.getState());
    assertEquals(c1.messageConsumptionCount, 100);
    assertEquals(c2.messageConsumptionCount, 100);
  }


  private Message createMessage() {
    return new JsonMessage("ID1", "{\"key\" : \"value\"}");
  }


  class BulkPublisher extends Thread {
    private AbstractQueue<Message> abstractQueue;
    private Map<String, Boolean> publisherCompletionNotifier;
    private String publisherName;
    private int count;
    BulkPublisher(String publisherName,AbstractQueue<Message> abstractQueue, Map<String, Boolean> publisherCompletionNotifier, int count) {
      this.abstractQueue = abstractQueue;
      this.publisherCompletionNotifier = publisherCompletionNotifier;
      this.publisherName = publisherName;
      this.count = count;
    }

    @SneakyThrows
    @Override
    public void run() {
      for (int i = 1; i <= count; i++) {
        Message message = createMessage();
        abstractQueue.enqueue(message);
      }
      publisherCompletionNotifier.put(publisherName,true);
    }
  }

  static class Consumer extends Thread{
    private int messageConsumptionCount = 0;
    public Consumer(
        AbstractQueueIterator<Message> iterator) {
      this.iterator =  iterator;
    }

    public int getMessageConsumptionCount() {
      return messageConsumptionCount;
    }
    AbstractQueueIterator<Message> iterator;

    public void run() {
      while (true) {
        iterator.next();
        messageConsumptionCount++;
      }
    }
  }

}