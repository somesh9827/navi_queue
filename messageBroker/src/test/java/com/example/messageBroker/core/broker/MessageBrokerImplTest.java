package com.example.messageBroker.core.broker;

import com.example.messageBroker.core.broker.iface.QueueManager;
import com.example.messageBroker.core.dto.Message;
import com.example.messageBroker.core.dto.SubscriberType;
import com.example.messageBroker.core.queue.AbstractQueue;
import com.example.messageBroker.core.queue.QueueType;
import com.example.messageBroker.core.dto.SubscriptionInfo;
import com.example.messageBroker.exception.ResourceAlreadyExistException;
import com.example.messageBroker.exception.ResourceNotExistException;
import com.example.messageBroker.exception.SubscriberAlreadyExistException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageBrokerImplTest {

  @Mock
  private  RestTemplate restTemplate;

  @Mock
  private QueueManager<Message> queueManager;

  @InjectMocks
  private MessageBrokerImpl messageBroker;


  @Before
  public void init() {
    messageBroker = new MessageBrokerImpl(queueManager, restTemplate);
  }


  private final String queueName = "defaultQueue";

  @Test
  public void testSubscribeWhenSubscriptionIsNotAlreadyExistThenCreateSubscriber() {
    when(queueManager.getQueue(queueName)).thenReturn(Optional.of(createAbstractQueue()));
    SubscriptionInfo subscriptionInfo = createSubscriptionInfo("default_id");
    messageBroker.subscribe(subscriptionInfo);
  }

  @Test(expected = SubscriberAlreadyExistException.class)
  public void testSubscribeWhenSubscriptionIsAlreadyExistThenThrowError() {
    when(queueManager.getQueue(queueName)).thenReturn(Optional.of(createAbstractQueue()));
    SubscriptionInfo subscriptionInfo = createSubscriptionInfo("default_id");
    messageBroker.subscribe(subscriptionInfo);
    //using same subscription info so it should throw error
    messageBroker.subscribe(subscriptionInfo);

  }

  @Test
  public void testUnsubscribeWhenSubscriptionIsAlreadyExistThenUnsubscribe() {
    when(queueManager.getQueue(queueName)).thenReturn(Optional.of(createAbstractQueue()));
    SubscriptionInfo subscriptionInfo = createSubscriptionInfo("default_id");
    messageBroker.subscribe(subscriptionInfo);
    messageBroker.unsubscribe(queueName, subscriptionInfo.getSubscriberId());
  }

  @Test(expected = ResourceNotExistException.class)
  public void testUnsubscribeWhenSubscriptionIsNotExistThenThrowError() {
    SubscriptionInfo subscriptionInfo = createSubscriptionInfo("default_id");
    messageBroker.unsubscribe(queueName, subscriptionInfo.getSubscriberId());
  }

  @Test
  public void testCreateQueueWhenQueueIsNotAlreadyExistThenCreateNewQueue() {
    when(queueManager.createQueue(queueName,QueueType.IN_MEMORY_QUEUE)).thenReturn(createAbstractQueue());
    messageBroker.createQueue(queueName, QueueType.IN_MEMORY_QUEUE);
  }

  @Test(expected = ResourceAlreadyExistException.class)
  public void testCreateQueueWhenQueueIsAlreadyExistThenThrowError() {
    when(queueManager.createQueue(queueName,QueueType.IN_MEMORY_QUEUE)).thenThrow(new ResourceAlreadyExistException());
    messageBroker.createQueue(queueName, QueueType.IN_MEMORY_QUEUE);
  }


  private SubscriptionInfo createSubscriptionInfo(String subscriberId) {
    return new SubscriptionInfo(queueName, "localhost:8091/subscription1/callback", HttpMethod.PUT,subscriberId,
        SubscriberType.HTTP_SUBSCRIBER);
  }

  private AbstractQueue<Message> createAbstractQueue() {
    return QueueFactory.createQueue(queueName, QueueType.IN_MEMORY_QUEUE);
  }


}
