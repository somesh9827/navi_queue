## Message Queuing System(https://codu.ai/coding-problem/navi-messaging-queue)

This project is implementation of Publisher-consumer queue. where 1 publisher produce the message to the queue and any number of consumer can read the messages.
Currently I have implemented Unordered, AppendOnly, Blocking, InMemory queue. but can be extendible to FIFO/Filesystem queue also. (I have not used any builtIn queue library)

### Queue Implementation Detail
 I have Implemented In-Memory queue which is AppendOnly, Blocking Queue i.e.
 1. Data can not be deleted from queue.(This is needed to support multiple Consumer).
 2. If Consumer has read all the data so queue.next() method will block the current thread and notified when new data added to Queue  

### Implementation Detail
  Project is divided into 2 sub-module
  1. messageBroker
  2. subscriber

 **MessageBroker** : Message broker module contain responsibility of creating queue, subscribe and unsubscribe for subscriber. It also contain Driver
  class which read input from text file  create queue, publish message, subscribe and unsubscribe subscriber using MessageBroker interface.(In real life producer will be a different service which call message broker api to produce message)
  MessageBroker Service Has Message broker class which expose following interface :
  
   1. **Create Queue** : This interface create new queue in system. Currently I am supoporting InMemoryQueue. At this time no consumer is created
   
   2. **subscribe** : This API create 1 Consumer thread which take list Iterator and Subscriper-service API,as input and call iterator.next() to consume new message. interator.next() is blocking call, so if new message is not present in queue it block the Thread.
                 whenever new message added in queue all blocking thread are notified ans consumer call Subscriber API with Message as Request Body.
                 
   3. **unsubscribe** : It shutdown the consumer.
   
   4. **getPublisher** : It return the publisher for specific queue I have used this method since we needed only 1 producer for each queue.

 **Subscriber Module**: Subscriber is a Normal API Service which listen to some port. Consumer call this service whenever new message present in Queue.

 **How to run :**
* Step 1 run subscriber service
 * go to subscriber module -> cd navi_queue/subscriber
 * mvn clean compile install
 * mvn spring-boot:run

* Step 2 : Run Messagebroker and producer Service
 * go to messageBroker module -> cd navi_queue/messageBroker
 * mvn clean compile install
 * mvn spring-boot:run

 **Note:**
Currently I am reading input from navi_queue/messageBroker/src/main/resources/input.txt file which has following format
1. Create Queue -> **create_queue {queue_name}**
2. Publish n Random Message -> **publish_random {queue_name} {message_count}**
3. Add Subscriber -> **subscribe {queue_name} {subscriber_id} {callback_url} {http_method}**
4. Unsubscribe -> **unsubscribe queue_1 Subscriber2**


