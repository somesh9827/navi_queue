package com.example.subscriber.controller;

import com.example.subscriber.dto.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/subscriber3")
@Slf4j
public class SubscriberController3 {

  @PutMapping("/callback")
  public ResponseEntity callback(@RequestBody Message message) {
    return new  ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    //log.info("subscriber3 : messageId = {}, data = {}", message.getId(), message.getData());
  }

}
