package com.example.subscriber.controller;

import com.example.subscriber.dto.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/subscriber1")
@Slf4j
public class SubscriberController1 {

  @PutMapping("/callback")
  public void callback(@RequestBody Message message) {
    log.info("subscriber1 : messageId = {}, data = {}", message.getId(), message.getData());
  }

}
