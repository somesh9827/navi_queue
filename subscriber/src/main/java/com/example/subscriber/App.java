package com.example.subscriber;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class,
    ManagementWebSecurityAutoConfiguration.class, DataSourceAutoConfiguration.class})
public class App implements ApplicationRunner {
  public static void main(String[] args) {
    SpringApplication.run(App.class, args);
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
    System.out.println("args " + args.getOptionNames());
  }
}
