package com.example.subscriber.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {
  private String id;
  private String data;
}
